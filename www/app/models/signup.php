<?php 
    class Signup {

        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function addSignup($Username, $Email,  $Password ) {
            $this->db->query("INSERT INTO Signup (UserName,Email,Password) VALUES (:Username,:Email, :Password)");
            $this->db->bind(":Username", $Username);
            $this->db->bind(":Password", $Password);
            $this->db->bind(":Email", $Email);
            if($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }
    }
?>