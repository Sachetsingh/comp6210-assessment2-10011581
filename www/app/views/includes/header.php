<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sachet</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Audiowide">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>css/styles.css"> 
</head>

<body>
    <nav class="navbar navbar-light navbar-expand-md">
        <div class="container-fluid"><a class="navbar-brand" href="#"></a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse float-left"
                id="navcol-1">
                <ul class="nav navbar-nav mx-auto">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="/Pages/Home"><strong>Home</strong></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="/Pages/About"><strong>About</strong></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="/Pages/Services"><strong>Services</strong></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="/Pages/FAQs"><strong>FAQs</strong></a></li>
                </ul>
                <li class="nav-item" role="presentation">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn " data-toggle="modal" data-target="#SignUp">
                            Sign Up
                        </button>
                    </li>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="intro"></div>
    </div>