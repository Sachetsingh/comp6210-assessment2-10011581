-- SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
-- SET time_zone = "+00:00";

-- Create Database if not hosted on azure

CREATE DATABASE IF NOT EXISTS `container-db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `container-db`;

-- --------------------------------------------------------

-- Table structure for table `tbl_songCollection`
-- DROP TABLE IF EXISTS `tbl_songCollection`;
-- DROP TABLE IF EXISTS `tbl_users`;
-- DROP TABLE IF EXISTS `Feedback`;

-- CREATE TABLE `tbl_songCollection` (
--   `ID` int(11) NOT NULL AUTO_INCREMENT,
--   `Song_Name` varchar(30) NOT NULL,
--   `Album_Name` varchar(30) NOT NULL,
--   `Artist_Name` varchar(30) NOT NULL,
--   PRIMARY KEY ( `ID` )
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 -- Table structure for table `Feedback`
--  DROP TABLE IF EXISTS `Feedback`;
--  CREATE TABLE `Feedback` (
--    `Feedback_ID` int(11) NOT NULL ,
--    `Login_ID` int(10)  NULL,
--    `First_Name` varchar(50) NULL ,
--    `Last_Name` varchar(50) NULL,
--    `Email` varchar(50) NOT NULL,
--    `Rating` int(10) NOT NULL,
--    `Message` varchar(255) NULL ,
--    PRIMARY KEY ( `Feedback_ID` )
--  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Table structure for table `tbl_users`
-- DROP TABLE IF EXISTS `tbl_users`;
-- CREATE TABLE `tbl_users` (
--   `ID` int(11) NOT NULL AUTO_INCREMENT,
--   `USERNAME` varchar(20) NOT NULL,
--   `PASSWRD` varchar(30) NOT NULL,
--   PRIMARY KEY ( `ID` )
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Clear data for table `tbl_songCollection`
-- TRUNCATE TABLE `tbl_songCollection`
-- INSERT INTO `tbl_songCollection` (`Song_Name`, `Album_Name`, `Artist_Name`) VALUES
-- ('Alone', 'DJ_Valdi', 'Alan_Walker'),
-- ('Psycho', 'Post_Malone', 'Louis_Bell'),
-- ('Closer', 'The_Chainsmokers', 'Collage'),
-- ("God’s_Plan", 'Scary_Hours', 'Drake'),
-- ('New_Rules', 'Dua_lipa', 'Dua_lipa'),
-- ('Paradise', 'Mylo Xyloto', 'Cold_Play'),
-- ('Work', 'Anti', 'Rihanna'),
-- ('Work', 'Anti', 'Rihanna'),
-- ('Work', 'Anti', 'Rihanna'),
-- ('Work', 'Anti', 'Rihanna');

 -- Clear data for table `Feedback`
 -- TRUNCATE TABLE `Feedback`
--  INSERT INTO `Feedback` (`Feedback_ID`, `Login_ID`, `First_Name`,`Last_Name`,`Email`,`Rating`,`Message`) VALUES
--   (1,1,'Sachet','Panwar','Sachet@gmail.com',7, 'This is a very good website'),
--   (2,NULL,NULL,NULL,'anonymous@gmail.com',6, 'This is a very good website'),
--   (3,6,'Frank',NULL,'frank@gmail.com',5, 'This is a very good website'),
--   (4,2,'Adam',NULL,'adam@gmail.com',8, 'This is a very good website'),
--   (5,NULL,NULL,NULL,'Barry@gmail.com',6, 'This is a very good website'),
--   (6,NULL,NULL,NULL,'Quenn@gmail.com',10, 'This is a very good website');

-- Clear data for table `tbl_users`
-- TRUNCATE TABLE `tbl_users`
-- INSERT INTO `tbl_users` (`USERNAME`, `PASSWRD`) VALUES ('Admin', 'admin123');
-- INSERT INTO `tbl_users` (`USERNAME`, `PASSWRD`) VALUES ('Sachet', 'Sachet123');
-- INSERT INTO `tbl_users` (`USERNAME`, `PASSWRD`) VALUES ('Jeff', 'Jeff123');
-- INSERT INTO `tbl_users` (`USERNAME`, `PASSWRD`) VALUES ('Jon', 'Jon123');
-- INSERT INTO `tbl_users` (`USERNAME`, `PASSWRD`) VALUES ('Ben', 'Ben123');
-- INSERT INTO `tbl_users` (`USERNAME`, `PASSWRD`) VALUES ('Sam', 'Sam123');


-- DROP TABLE IF EXISTS pages;
    -- CREATE TABLE pages (
    -- ID INT(11) AUTO_INCREMENT,
    -- Name varchar(100) NOT NULL,
    -- CONTENT TEXT NULL,
    -- PRIMARY KEY (ID)
    -- )AUTO_INCREMENT = 1;

--     Clear data for table pages
    --  TRUNCATE TABLE pages
    --  INSERT INTO pages (  Name ,  CONTENT ) VALUES
    -- ('Home','<div>
    --     <div class="container">
    --         <div class="row">
    --             <div class="col-md-12">
    --                 <h2 class="text-center">Sky Fly - Music Band</h2>
    --             </div>
    --         </div>
    --     </div>
    -- </div>
    -- <div class="row">
    --     <div class="col">
    --         <p><br>Music is an art form and cultural activity whose medium is sound organized in time. The common elements of music are pitch (which governs melody and harmony), rhythm (and its associated concepts tempo, meter, and articulation), dynamics
    --             (loudness and softness), and the sonic qualities of timbre and texture (which are sometimes termed the "color" of a musical sound). Different styles or types of music may emphasize, de-emphasize or omit some of these elements.nized in
    --             time.<br><br><br></p>
    --     </div>
    --     <div class="col">
    --         <p><br>Music is an art form and cultural activity whose medium is sound organized in time. The common elements of music are pitch (which governs melody and harmony), rhythm (and its associated concepts tempo, meter, and articulation), dynamics
    --             (loudness and softness), and the sonic qualities of timbre and texture (which are sometimes termed the "color" of a musical sound). Different styles or types of music may emphasize, de-emphasize or omit some of these elements.nized in
    --             time.<br><br><br></p>
    --     </div>
    -- </div>
    -- <div class="testimonials-clean"></div>
    -- <div></div>
    -- <div class="testimonials-clean"></div>
    -- <div class="testimonials-clean">
    --     <div class="container">
    --         <div class="intro">
    --             <h2 class="text-center">Testimonials </h2>
    --             <p class="text-center">Our customers love us! Read what they have to say below. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae.</p>
    --         </div>
    --         <div class="row people">
    --             <div class="col-md-4 col-lg-4 item" id="testimonal">
    --                 <div class="box">
    --                     <p class="description">This website is very helpful to play music and to listen this band you just have to log in and select song whatever you like to play.</p>
    --                 </div>
    --                 <div class="author"><img class="rounded-circle" src="/img/1.jpg">
    --                     <h5 class="name">Ben</h5>
    --                     <p class="title">CEO of Company</p>
    --                 </div>
    --             </div>
    --             <div class="col-md-4 col-lg-4 item" id="testimonal">
    --                 <div class="box">
    --                     <p class="description">This website is very helpful to play music and to listen this band you just have to log in and select song whatever you like to play.</p>
    --                 </div>
    --                 <div class="author"><img class="rounded-circle" src="/img/3.jpg">
    --                     <h5 class="name">Jeff</h5>
    --                     <p class="title">Founder of business</p>
    --                 </div>
    --             </div>
    --             <div class="col-md-4 col-lg-4 item" id="testimonal">
    --                 <div class="box">
    --                     <p class="description">This website is very helpful to play music and to listen this band you just have to log in and select song whatever you like to play.</p>
    --                 </div>
    --                 <div class="author"><img class="rounded-circle" src="/img/2.jpg">
    --                     <h5 class="name">Sachet</h5>
    --                     <p class="title">Owner of Company</p>
    --                 </div>
    --             </div>
    --         </div>
    --     </div>
    -- </div>'),
    -- ('About','<h1 class="text-center">Policy</h1>
    -- <div>
    --     <div class="container">
    --         <div class="row">
    --             <div class="col-md-12">
    --                 <h2>Equal Opportunity<br></h2>
    --                 <p>Equal opportunity laws are rules that promote fair treatment in the workplace. Most organizations implement equal opportunity policies anti-discrimination and affirmative action policies, for example -- to encourage unprejudiced behavior
    --                     within the workplace.<br></p>
    --                 <h2>Off Time</h2>
    --                 <p>Attendance policies set rules and guidelines surrounding employee adherence to work schedules. Attendance policies define how employees may schedule time off or notify superiors of an absence or late arrival. This policy also sets
    --                     forth the consequences for failing to adhere to a schedule.<br></p>
    --             </div>
    --         </div>
    --     </div>
    -- </div>'),
    -- ('Services','    <form method="POST" id="songForm" action="/Pages/getTheSong/">
    --     <h1 class="text-center">Services</h1>
    --     <h3 class="text-center">Songs Collection</h3>
    --     <div class="d-flex justify-content-center"><input type="Search" name="song" value=""></div>
    --     <div class="d-flex justify-content-center"><input type="submit" class="btn btn-primary" type="button" value="Button"></div>
    -- </form>

    -- <div class="row">
    -- </div>

    -- <div class="row">
    --     <div class="col-sm"><iframe width="100%" height="315" allowfullscreen="" frameborder="0" src="https://www.youtube.com/embed/1G4isv_Fylg?rel=0"></iframe></div>
    --     <div class="col-sm">
    --         <h5 class="text-center">Weather</h5>
    --         <a href="https://www.accuweather.com/en/nz/tauranga/246959/weather-forecast/246959" class="aw-widget-legal">
    --             <!--
    --             By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at https://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at https://www.accuweather.com/en/privacy.
    --             -->
    --             </a><div id="awcc1523503877310" class="aw-widget-current"  data-locationkey="246959" data-unit="c" data-language="en-us" data-useip="false" data-uid="awcc1523503877310"></div><script type="text/javascript" src="https://oap.accuweather.com/launch.js"></script>
    --     </div>
    -- </div>'),
    -- ('FAQs','<h2 class="text-center">Frequently asked questions</h2>
    -- <div>
    --     <div class="container">
    --         <div class="row">
    --             <div class="col-md-12">
    --                 <h2>Ques 1. How to write your name ?<br></h2>
    --                 <p>Answer: Equal opportunity laws are rules that promote fair treatment in the workplace. Most organizations implement equal opportunity policies anti-discrimination and affirmative action policies, for example -- to encourage unprejudiced
    --                     behavior within the workplace.<br></p>
    --                 <h2>Ques 2. How to Sign in you ID ?</h2>
    --                 <p>Answer: Attendance policies set rules and guidelines surrounding employee adherence to work schedules. Attendance policies define how employees may schedule time off or notify superiors of an absence or late arrival. This policy also
    --                     sets forth the consequences for failing to adhere to a schedule.<br></p>
    --             </div>
    --         </div>
    --     </div>
    -- </div>
    -- <div class="contact-clean">
    --     <h1 class="text-center">Feedback Form</h1>
    --     <form method="post">
    --         <h2 class="text-center">Give your feedback here</h2>
    --         <div class="form-group"><input class="form-control" type="text" name="name" placeholder="Name"></div>
    --         <div class="form-group"><input class="form-control is-invalid" type="email" name="email" placeholder="Email"><small class="form-text text-danger">Please enter a correct email address.</small></div>
    --         <div class="form-group"><textarea class="form-control" rows="14" name="message" placeholder="Message"></textarea></div>
    --         <div class="form-group"><button class="btn btn-primary" type="submit">send </button></div>
    --     </form>
    -- </div>');
    -- select * from pages

    -- DROP TABLE IF EXISTS Contact;

    -- CREATE TABLE Contact (
    -- ID INT(11) AUTO_INCREMENT,
    --     FNAME VARCHAR(40) ,
    --     Email VARCHAR(100),
    --     Message TEXT(500),
    --     PRIMARY KEY (ID)
    -- ) AUTO_INCREMENT = 1;
     -- Clear data for table  Contact 
     -- TRUNCATE TABLE  Contact 


    -- select * from Contact

    --  -- Table structure for table  Feedback 
    --  DROP TABLE IF EXISTS  Feedback ;
    --  CREATE TABLE  Feedback  (
    --     Feedback_ID  int(11) AUTO_INCREMENT ,
    --     FNAME  varchar(50) NULL ,
    --     Email  varchar(50) NOT NULL,
    --     Message  varchar(255) NULL ,
    --    PRIMARY KEY (  Feedback_ID  )
    --  ) AUTO_INCREMENT = 1;
    --  -- Clear data for table  Feedback 
    --  TRUNCATE TABLE  Feedback 
    --  INSERT INTO  Feedback  (  FNAME , Email , Message ) VALUES
    --   (1,'Sachet','panwarsachet@gmail.com','This is a music website');

    -- select * from contact


    --  DROP TABLE IF EXISTS  Signup ;
    -- Table structure for table Signup 
    --     CREATE TABLE  Signup  (
    --     SignupID  int(10) AUTO_INCREMENT ,
    --     UserName  varchar(30) NULL ,
    --     Email  varchar(50) NOT NULL,
    --     Password  varchar(30) NULL ,
    --     PRIMARY KEY (  SignupID  )
    --  )  AUTO_INCREMENT = 1;
    --     Clear data for table  Signup 
        -- TRUNCATE TABLE Signup 
    --     INSERT INTO  Signup  (  Username , Email , Password ) VALUES
    --    (1,'Sachet','panwarsachet@gmail.com','Sachet123);

    -- select * from Signup

